// ignore_for_file: prefer_const_constructors

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_test_2/constants.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var opacity = 0.3;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize: MainAxisSize.max,
          // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              'assets/3.jpg',
              width: width,
              height: height * 0.26,
              fit: BoxFit.contain,
            ),
            // Container(
            //   height: height * 0.5,
            //   width: width,
            //   color: Colors.orange,
            // ),
            Align(
              alignment: Alignment.center,
              child: Text(
                appName,
                style: TextStyle(fontSize: 23, fontWeight: FontWeight.w600),
              ),
            ),
            Center(
              child: Text(
                slogan,
                style: TextStyle(color: Colors.green),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Container(
                child: Text(
                  "  Login",
                  style: TextStyle(color: Colors.green, fontSize: 22),
                ),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      // stops: [0.3, 0.7],
                      // begin: Alignment.topCenter,
                      // end: Alignment.bottomCenter,
                      colors: [
                        Colors.green.withOpacity(0.3),
                        Colors.lightGreen.withOpacity(0.1)
                      ],
                    ),
                    border: Border(
                        left: BorderSide(
                      color: Colors.green,
                      width: 5,
                    ))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextField(
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green)),
                      prefixIcon: Icon(
                        Icons.email,
                        color: Colors.green,
                      ),
                      labelText: "EMAIL ADDRESS",
                      labelStyle:
                          TextStyle(color: Colors.lightGreen, fontSize: 15))),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green)),
                      prefixIcon: Icon(
                        Icons.lock_open,
                        color: Colors.green,
                      ),
                      labelText: "PASSWORD",
                      labelStyle:
                          TextStyle(color: Colors.lightGreen, fontSize: 15))),
            ),
            Align(
                alignment: Alignment.centerRight,
                child: FlatButton(
                    onPressed: () {}, child: Text("Forgot Password?"))),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Center(
                  child: SizedBox(
                      height: height * 0.08,
                      width: width - 30,
                      child: FlatButton(
                          color: Colors.green,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          onPressed: () {},
                          child: Text(
                            "Login to account",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                letterSpacing: 0.5,
                                fontWeight: FontWeight.w900),
                          )))),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Dont have an account"),
                  FlatButton(onPressed: () {}, child: Text("Create Account"))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
