import 'package:flutter/material.dart';
import 'package:flutter_test_2/constants.dart';

import 'login_screen.dart';

void main() {
  runApp(InitialScreen());
}

class InitialScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
      title: appName,
    );
  }
}
