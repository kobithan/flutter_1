import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String placeholder = "";
  int i = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: Center(child: Text("value $i")),
          appBar: AppBar(
            title: Text("kobi"),
            centerTitle: true,
          ),
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add_circle_outline_rounded),
              onPressed: () {
                setState(() {
                  i = i + 1;
                  placeholder = "clicked";
                });
                print(placeholder);
              }),
        ));
  }
}
